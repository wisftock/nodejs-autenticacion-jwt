const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categoriaSchema = new Schema({
  description: {
    type: String,
    unique: true,
    required: [true, 'Description is required'],
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'usuario',
  },
});

const Categoria = mongoose.model('categoria', categoriaSchema);
module.exports = Categoria;
