const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productoSchema = new Schema({
  name: {
    type: String,
    required: [true, 'The name is necessary'],
  },
  priceUni: {
    type: Number,
    required: [true, 'The unit price is required'],
  },
  description: {
    type: String,
    required: false,
  },
  available: {
    type: Boolean,
    required: true,
    default: true,
  },
  category: {
    type: Schema.Types.ObjectId,
    ref: 'categoria',
    required: true,
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: 'usuario',
  },
  img: {
    type: String,
    required: false,
  },
});

const Producto = mongoose.model('producto', productoSchema);
module.exports = Producto;
