const mongoose = require('mongoose');
const mongooseUniqueValidator = require('mongoose-unique-validator');

let rolesValidos = {
  values: ['ADMIN_ROLE', 'USER_ROLE'],
  message: '{VALUE} is not a valid role',
};

const Schema = mongoose.Schema;

const usuarioSchema = new Schema({
  name: {
    type: String,
    required: [true, 'The name is required'],
  },
  email: {
    type: String,
    required: true,
    unique: [true, 'The email is required'],
  },
  password: {
    type: String,
    required: [true, 'The password is required'],
  },
  img: {
    type: String,
    required: false,
  },
  role: {
    type: String,
    default: 'USER_ROLE',
    enum: rolesValidos,
  },
  state: {
    type: Boolean,
    default: true,
  },
  google: {
    type: Boolean,
    default: false,
  },
});

usuarioSchema.methods.toJSON = function () {
  let user = this;
  let userObject = user.toObject();
  delete userObject.password;

  return userObject;
};
usuarioSchema.plugin(mongooseUniqueValidator, {
  message: '{PATH} must be unique',
});

const Usuario = mongoose.model('usuario', usuarioSchema);
module.exports = Usuario;
