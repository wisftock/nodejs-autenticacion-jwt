const Usuario = require('../models/usuario');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

const SignIn = (req, res) => {
  Usuario.findOne({ email: req.body.email }, (err, userDB) => {
    if (err) {
      res.status(500).send({ error: err });
    } else {
      if (!userDB) {
        res
          .status(404)
          .send({ message: '(Usuario) o contraseñas incorrectos' });
      } else {
        if (!bcrypt.compareSync(req.body.password, userDB.password)) {
          res.status(400).send({ message: 'Passwords are not the same' });
        } else {
          let token = jwt.sign({ userDB }, process.env.SEEND_TOKEN, {
            expiresIn: process.env.CADUCIDAD_TOKEN,
          });
          res.status(200).json({ userDB, token });
        }
      }
    }
  });
};

// Configuracion de google
async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
    // Or, if multiple clients access the backend:
    //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
  });
  const payload = ticket.getPayload();
  return {
    name: payload.name,
    email: payload.email,
    img: payload.picture,
    google: true,
  };
}

const GoogleNode = async (req, res) => {
  let token = req.body.idtoken;
  // console.log(token);
  let googleUser = await verify(token).catch((err) => {
    return res.status(403).json({
      error: err,
    });
  });

  // res.json({
  //   usuario: googleUser,
  // });

  Usuario.findOne({ email: googleUser.email }, (err, userDB) => {
    if (err) {
      return res.status(500).send({ error: err });
    }

    if (userDB) {
      if (userDB.google === false) {
        return res
          .status(400)
          .send({ message: 'Debe usar su autenticacion normal' });
      } else {
        let token = jwt.sign({ userDB }, process.env.SEEND_TOKEN, {
          expiresIn: process.env.CADUCIDAD_TOKEN,
        });

        return res.json({ userDB, token });
      }
    } else {
      // si el usuario no existe en la base de datos
      let usuario = new Usuario();
      usuario.name = googleUser.name;
      usuario.email = googleUser.email;
      usuario.img = googleUser.img;
      usuario.google = true;
      usuario.password = ':D';

      usuario.save((err, userDB) => {
        if (err) {
          return res.status(500).send({ error: err });
        }

        let token = jwt.sign({ userDB }, process.env.SEEND_TOKEN, {
          expiresIn: process.env.CADUCIDAD_TOKEN,
        });

        return res.json({ userDB, token });
      });
    }
  });
};

module.exports = {
  SignIn,
  GoogleNode,
};
