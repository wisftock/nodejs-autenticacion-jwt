const Usuario = require('../models/usuario');
const bcrypt = require('bcryptjs');
const _ = require('underscore');

const userGet = (req, res) => {
  let desde = req.query.desde || 0;
  desde = Number(desde);

  let hasta = req.query.hasta || 5;
  hasta = Number(hasta);

  Usuario.find({ state: true }, 'name email state img role google')
    .skip(desde)
    .limit(hasta)
    .exec((err, userDB) => {
      if (err) {
        res.status(500).send({ error: err });
      } else {
        if (!userDB) {
          res.status(404).send({ message: 'No users' });
        } else {
          Usuario.countDocuments({ state: true }, (err, conteo) => {
            res.status(200).json({ userDB, users: conteo });
          });
        }
      }
    });
};

const userPost = (req, res) => {
  const { name, email, password, role } = req.body;
  let usuario = new Usuario({
    name: name,
    email: email,
    password: bcrypt.hashSync(password, 10),
    role: role,
  });
  usuario.save((err, userDB) => {
    if (err) {
      res.status(400).send({ error: err });
    } else {
      if (!userDB) {
        res.status(404).send({ message: 'No user found' });
      } else {
        res.status(200).json({ message: 'Added correctly', userDB });
      }
    }
  });
};

const userUpdate = (req, res) => {
  const body = _.pick(req.body, ['name', 'email', 'img', 'role', 'state']);

  Usuario.findByIdAndUpdate(
    req.params.id,
    body,
    { new: true, runValidators: true },
    (err, userDB) => {
      if (err) {
        res.status(400).send({ error: err });
      } else {
        if (!userDB) {
          res.status(404).send({ message: 'Username does not exist' });
        } else {
          res.status(200).json({ message: 'Update successfully', userDB });
        }
      }
    }
  );
};

const userDelete = (req, res) => {
  let id = req.params.id;
  Usuario.findByIdAndUpdate(
    id,
    { state: false },
    { new: true },
    (err, userDB) => {
      if (err) {
        res.status(400).send({ error: err });
      } else {
        if (!userDB) {
          res.status(404).send({ message: 'Username does not exist' });
        } else {
          res.status(200).json({ userDB });
        }
      }
    }
  );
};

module.exports = {
  userPost,
  userGet,
  userUpdate,
  userDelete,
};
