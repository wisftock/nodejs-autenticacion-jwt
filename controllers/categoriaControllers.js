const Categoria = require('../models/categoria');

const ListCategory = (req, res) => {
  Categoria.find({})
    .sort('description') /* ordenar por descripcion */
    .populate(
      'user',
      'name email'
    ) /* mostrar al usuario que le hace referente */
    .exec((err, userDB) => {
      if (err) {
        res.status(500).send(err);
      } else {
        if (!userDB) {
          res.status(400).send({ message: 'The category does nit exist' });
        } else {
          Categoria.countDocuments({}, (err, conteo) => {
            res.status(200).json({ userDB, Category: conteo });
          });
        }
      }
    });
};

const ListCategoryID = (req, res) => {
  Categoria.findById(req.params.id, (err, userDB) => {
    if (err) {
      res.status(500).send({ error: err });
    } else {
      if (!userDB) {
        res.status(404).send({ message: 'There is no category' });
      } else {
        res.status(200).json(userDB);
      }
    }
  });
};

const PostCategory = (req, res) => {
  let categoria = new Categoria({
    description: req.body.description,
    user: req.userDB._id,
  });

  categoria.save((err, userDB) => {
    if (err) {
      return res.status(500).send({ error: err });
    } else {
      if (!userDB) {
        res.status(400).send({ message: 'There is no category' });
      } else {
        res.status(200).json({ userDB, message: 'Category created' });
      }
    }
  });
};

const UpdateCategory = (req, res) => {
  let body = req.body;

  Categoria.findByIdAndUpdate(
    req.params.id,
    { description: body.description },
    { new: true, runValidators: true },
    (err, userDB) => {
      if (err) {
        return res.status(500).send({ error: err });
      } else {
        if (!userDB) {
          res.status(404).send({ message: 'There is no category' });
        } else {
          // console.log(userDB);
          res.status(200).json(userDB);
        }
      }
    }
  );
};

const DeleteCategory = (req, res) => {
  Categoria.findByIdAndRemove(req.params.id, (err, userDB) => {
    if (err) {
      res.status(500).send(err);
    } else {
      if (!userDB) {
        res.status(400).send({ err, message: 'ID does not exist' });
      } else {
        res.status(200).json({ message: 'Delete category' });
      }
    }
  });
};

module.exports = {
  ListCategory,
  ListCategoryID,
  PostCategory,
  UpdateCategory,
  DeleteCategory,
};
