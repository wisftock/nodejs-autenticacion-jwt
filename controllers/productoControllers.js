const Producto = require('../models/producto');

const ListProduct = (req, res) => {
  let desde = req.query.desde || 0;
  desde = new Number(desde);

  let hasta = req.query.hasta || 10;
  hasta = new Number(hasta);

  Producto.find({ available: true })
    .skip(desde)
    .limit(hasta)
    .populate('user', 'name email')
    .populate('category', 'description')
    .exec((err, userDB) => {
      if (err) {
        return res.status(500).send({ err });
      } else {
        if (!userDB) {
          res.status(400).send({ err, message: 'No products' });
        } else {
          Producto.countDocuments({ available: true }, (err, conteo) => {
            res.status(200).json({ userDB, producto: conteo });
          });
        }
      }
    });
};

const ListProductID = (req, res) => {
  Producto.findById(req.params.id)
    .populate('user', 'name email')
    .populate('category', 'description')
    .exec((err, userDB) => {
      if (err) {
        return res.status(500).send(err);
      } else {
        if (!userDB) {
          res.status(400).send({ message: 'The product does not exist' });
        } else {
          res.status(200).send(userDB);
        }
      }
    });
};

const SearchListProduct = (req, res) => {
  let termino = req.params.termino;
  let regex = new RegExp(termino, 'i'); // se usa i para que busque mayusculas y minusculas
  Producto.find({ name: regex })
    .populate('category', 'description')
    .exec((err, userDB) => {
      if (err) {
        return res.status(500).send({ err });
      } else {
        if (!userDB) {
          res.status(400).send({ message: 'Product not found' });
        } else {
          res.status(200).json(userDB);
        }
      }
    });
};

const PostProduct = (req, res) => {
  let producto = new Producto({
    name: req.body.name,
    priceUni: req.body.priceUni,
    description: req.body.description,
    available: req.body.available,
    category: req.body.category,
    user: req.userDB._id,
  });
  producto.save((err, userDB) => {
    if (err) {
      return res.status(500).send({ err });
    } else {
      if (!userDB) {
        res.status(400).send({ message: 'The product does not exist' });
      } else {
        res.status(200).json({ userDB, message: 'Added product' });
      }
    }
  });
};

const PutProduct = (req, res) => {
  Producto.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      priceUni: req.body.priceUni,
      description: req.body.description,
      available: req.body.available,
      category: req.body.category,
    },
    { new: true },
    (err, userDB) => {
      if (err) {
        return res.status(500).send({ err });
      } else {
        if (!userDB) {
          res.status(400).send({ message: 'The product does not exist' });
        } else {
          res.status(200).json({ userDB, message: 'Updated product' });
        }
      }
    }
  );
};

const DeleteProduct = (req, res) => {
  Producto.findByIdAndUpdate(
    req.params.id,
    { available: false },
    { new: true },
    (err, userDB) => {
      if (err) {
        return res.status(500).send({ err });
      } else {
        if (!userDB) {
          res.status(400).send({ message: 'The product does not exist' });
        } else {
          res.status(200).json({ userDB, message: 'Delete product' });
        }
      }
    }
  );
};

module.exports = {
  ListProduct,
  ListProductID,
  SearchListProduct,
  PostProduct,
  PutProduct,
  DeleteProduct,
};
