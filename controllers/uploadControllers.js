// const express = require('express');
// const fileUpload = require('express-fileupload');
// const router = express.Router();
// router.use(fileUpload());
const { Usuario, Producto } = require('../models');
const fs = require('fs');
const path = require('path');

const CargaArchivo = (req, res) => {
  if (!req.files) {
    return res.status(400).send({ message: 'No files were uploaded.' });
  }
  // validar tipo
  let tiposValidos = ['productos', 'usuarios'];
  if (tiposValidos.indexOf(req.params.carpeta) < 0) {
    return res.status(400).json({
      message: `The allowed types are ${tiposValidos.join(',')}`,
    });
  }

  let archivo = req.files.archivoFile;
  let archivoName = archivo.name.split('.');
  let extension = archivoName[archivoName.length - 1];

  // extenciones permitidas
  const extenciones = ['png', 'jpg', 'jpeg', 'gif'];
  if (extenciones.indexOf(extension) < 0) {
    return res.status(400).json({
      message: `The allowed extensions are ${extenciones.join(',')}`,
      ext: extension,
    });
  }

  // cambiar nombre al archivo
  let nombreArchivo = `${
    req.params.id
  }-${new Date().getMilliseconds()}.${extension}`;

  archivo.mv(`upload/${req.params.carpeta}/${nombreArchivo}`, (err) => {
    if (err) return res.status(500).json(err);

    // res.json({ message: 'File uploaded!' });
    if (req.params.carpeta === 'usuarios') {
      imagenUsuario(req.params.id, res, nombreArchivo);
    }
    if (req.params.carpeta === 'productos') {
      imagenProducto(req.params.id, res, nombreArchivo);
    }
  });
};

function imagenUsuario(id, res, nombreArchivo) {
  // console.log(nombreArchivo);
  Usuario.findById(id, (err, userDB) => {
    if (err) {
      borrarArchivo('usuarios', nombreArchivo);
      return res.status(500).send({ err });
    }

    if (!userDB) {
      borrarArchivo('usuarios', nombreArchivo);
      return res.status(400).send({ err: 'Username does not exist' });
    }

    // let pathImage = path.resolve(__dirname, `../upload/usuarios/${userDB.img}`);
    // console.log(pathImage);
    // if (fs.existsSync(pathImage)) {
    //   fs.unlinkSync(pathImage);
    // }

    borrarArchivo('usuarios', userDB.img);
    userDB.img = nombreArchivo;
    // console.log('user', userDB);

    userDB.save((err, usersDB) => {
      res.status(200).json(usersDB);
    });
  });
}

function imagenProducto(id, res, nombreArchivo) {
  Producto.findById(id, (err, userDB) => {
    if (err) {
      borrarArchivo('productos', nombreArchivo);
      return res.status(500).send({ err });
    }
    if (!userDB) {
      borrarArchivo('productos', nombreArchivo);
      return res.status(400).send({ err: 'The product does not exist' });
    }
    borrarArchivo('productos', userDB.img);
    userDB.img = nombreArchivo;
    // console.log(userDB);

    userDB.save((err, usersDB) => {
      res.status(200).json(usersDB);
    });
  });
}

function borrarArchivo(carpeta, nombreImagen) {
  let pathImage = path.resolve(
    __dirname,
    `../upload/${carpeta}/${nombreImagen}`
  );
  if (fs.existsSync(pathImage)) {
    fs.unlinkSync(pathImage);
  }
  // console.log(pathImage);
}

const VerArchivo = (req, res) => {
  // console.log(req.params);
  let pathImage = path.resolve(
    __dirname,
    `../upload/${req.params.carpeta}/${req.params.id}`
  );
  // let pathImage = `./upload/${req.params.carpeta}/${req.params.img}`;

  if (fs.existsSync(pathImage)) {
    res.sendFile(pathImage);
  } else {
    let nopathImage = path.resolve(__dirname, '../assets/no-image.jpg');
    res.sendFile(nopathImage);
  }

  // console.log(pathImage);
  // res.sendFile(nopathImage);
  // console.log(__dirname);
};

module.exports = { CargaArchivo, VerArchivo };
