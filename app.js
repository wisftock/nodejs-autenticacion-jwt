require('./configs/configs');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const routes = require('./routes');
const app = express();

// habilitar la carpeta public
app.use(express.static(path.resolve(__dirname, './public')));
// console.log(__dirname + '/public');

// conexion a la base de datos
mongoose.Promise = global.Promise;
mongoose.set('useFindAndModify', false);
mongoose
  .connect(process.env.URLDB, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('conectado a la base de datos'))
  .catch(() => console.log('error de conexion'));

// leer datos del body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// rutas
app.use('/', routes);

//
app.listen(process.env.PORT, () => {
  console.log(`http://localhost:${process.env.PORT}`);
});

// mongodb+srv://kras:<password>@cluster0.z0v9j.mongodb.net/test
