const express = require('express');
const fileUpload = require('express-fileupload');
const uploadControllers = require('../controllers/uploadControllers');
const {
  verificaTokenImagen,
  verificaToken,
} = require('../middlewares/auntenticacion');
const router = express.Router();

router.use(fileUpload());

router.put(
  '/upload/:carpeta/:id',
  [verificaToken],
  uploadControllers.CargaArchivo
);
router.get(
  '/viewImage/:carpeta/:id',
  [verificaTokenImagen],
  uploadControllers.VerArchivo
);
module.exports = router;
