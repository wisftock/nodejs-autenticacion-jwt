const express = require('express');
const categoriaControllers = require('../controllers/categoriaControllers');
const {
  verificaToken,
  verificarAdmin_Role,
} = require('../middlewares/auntenticacion');

const router = express.Router();

router.get('/categoria', [verificaToken], categoriaControllers.ListCategory);
router.get(
  '/categoria/:id',
  [verificaToken],
  categoriaControllers.ListCategoryID
);
router.post(
  '/categoriaPost',
  [verificaToken],
  categoriaControllers.PostCategory
);
router.put(
  '/categoriaUpdate/:id',
  [verificaToken],
  categoriaControllers.UpdateCategory
);
router.delete(
  '/categoriaDelete/:id',
  [verificaToken, verificarAdmin_Role],
  categoriaControllers.DeleteCategory
);

module.exports = router;
