const express = require('express');
const router = express.Router();

router.use(require('./usuario'));
router.use(require('./login'));
router.use(require('./categoria'));
router.use(require('./producto'));
router.use(require('./images'));
module.exports = router;
