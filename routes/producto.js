const express = require('express');
const productoControllers = require('../controllers/productoControllers');
const { verificaToken } = require('../middlewares/auntenticacion');

const router = express.Router();

router.get('/producto', [verificaToken], productoControllers.ListProduct);
router.get('/producto/:id', [verificaToken], productoControllers.ListProductID);
router.get(
  '/producto/search/:termino',
  [verificaToken],
  productoControllers.SearchListProduct
);
router.post('/productoPost', [verificaToken], productoControllers.PostProduct);
router.put(
  '/productoUpdate/:id',
  [verificaToken],
  productoControllers.PutProduct
);
router.delete(
  '/productoDelete/:id',
  [verificaToken],
  productoControllers.DeleteProduct
);

module.exports = router;
