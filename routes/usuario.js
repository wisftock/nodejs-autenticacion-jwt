const express = require('express');
const usuarioControllers = require('../controllers/usuarioControllers');
const {
  verificaToken,
  verificarAdmin_Role,
} = require('../middlewares/auntenticacion');

const router = express.Router();

router.get('/usuario', verificaToken, usuarioControllers.userGet);
router.post(
  '/usuario',
  [verificaToken, verificarAdmin_Role],
  usuarioControllers.userPost
);
router.put(
  '/usuario/:id',
  [verificaToken, verificarAdmin_Role],
  usuarioControllers.userUpdate
);
router.delete(
  '/usuario/:id',
  [verificaToken, verificarAdmin_Role],
  usuarioControllers.userDelete
);
module.exports = router;
