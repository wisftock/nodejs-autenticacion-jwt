const express = require('express');
const loginControllers = require('../controllers/loginControllers');
const router = express.Router();

router.post('/login', loginControllers.SignIn);
router.post('/google', loginControllers.GoogleNode);
module.exports = router;
