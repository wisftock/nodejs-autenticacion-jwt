const jwt = require('jsonwebtoken');

//  ====== validando token =====
const verificaToken = (req, res, next) => {
  let token = req.get('Authorization');

  jwt.verify(token, process.env.SEEND_TOKEN, (err, decoded) => {
    if (err) {
      res.status(401).send({ error: err });
    }
    req.userDB = decoded.userDB;

    next();
  });
};

const verificarAdmin_Role = (req, res, next) => {
  let usuario = req.userDB;

  if (usuario.role === 'ADMIN_ROLE') {
    next();
  } else {
    return res.json({ error: 'El usuario no es administrador' });
  }
  //   console.log(usuario);
};

//  ====== validando token para imagen =====
const verificaTokenImagen = (req, res, next) => {
  let token = req.query.token;
  jwt.verify(token, process.env.SEEND_TOKEN, (err, decoded) => {
    if (err) {
      res.status(401).send({ error: err });
    }
    req.userDB = decoded.userDB;

    next();
  });
};

module.exports = {
  verificaToken,
  verificarAdmin_Role,
  verificaTokenImagen,
};
