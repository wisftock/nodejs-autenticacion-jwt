// ===== PUERTO =====
process.env.PORT = process.env.PORT || 3000;

// ===== ENTORNO =====
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// ===== BASE DE DATOS =====
let urlDB;
if (process.env.NODE_ENV === 'dev') {
  urlDB = 'mongodb://localhost:27017/nodecafe';
} else {
  urlDB = process.env.MONGO_URI;
}
process.env.URLDB = urlDB;

// ===== JWT =====
// ===== fecha de vencimiento =====
// process.env.CADUCIDAD_TOKEN = 60 * 60 * 24 * 30;
process.env.CADUCIDAD_TOKEN = '48h';

// ===== seed de autenticacion =====
process.env.SEEND_TOKEN = process.env.SEEND_TOKEN || 'nodesecretkey_token';

// ===== GOOGLE Client ID =====
process.env.CLIENT_ID =
  process.env.CLIENT_ID ||
  '314410151541-17p3jl2892bpcq3uka82qj0umfkah8qe.apps.googleusercontent.com';
